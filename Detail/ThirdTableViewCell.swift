//
//  ThirdTableViewCell.swift
//  HW-003-Youtube
//
//  Created by Ratanak Phang on 12/10/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit

class ThirdTableViewCell: UITableViewCell {

    @IBOutlet weak var detailChannelImageView: UIImageView!
    @IBOutlet weak var detailChannelName: UILabel!
    @IBOutlet weak var imageSubscribe: CustomUIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


