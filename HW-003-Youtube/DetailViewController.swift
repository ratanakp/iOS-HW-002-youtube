//
//  DetailViewController.swift
//  HW-003-Youtube
//
//  Created by Ratanak Phang on 12/9/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    var getImage = UIImage()
    var detailContent = CellData()
    
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var detailTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.detailTableView.delegate = self
        self.detailTableView.dataSource = self
        
        //self.detailImageView.image = getImage
        
        //detailImageView.layer.borderWidth = 2
        
        self.detailImageView.image = detailContent.mainImage
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = Bundle.main.loadNibNamed("FirstTableViewCell", owner: self, options: nil)?.first as! FirstTableViewCell
            
            cell.firstTitle.text = detailContent.title
            cell.firstView.text = detailContent.totalViews
            
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            
            return cell
            
        } else if indexPath.row == 1 {
            let cell = Bundle.main.loadNibNamed("SecondTableViewCell", owner: self, options: nil)?.first as! SecondTableViewCell
            
            return cell
        } else {
            let cell = Bundle.main.loadNibNamed("ThirdTableViewCell", owner: self, options: nil)?.first as! ThirdTableViewCell
            
            cell.detailChannelName.text = detailContent.channelName
            cell.imageSubscribe.image = #imageLiteral(resourceName: "logo")
            cell.detailChannelImageView.image = detailContent.channelImage
            
            return cell
        }
    }
}


@IBDesignable
class CustomDetailImageView: UIImageView {
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
}






