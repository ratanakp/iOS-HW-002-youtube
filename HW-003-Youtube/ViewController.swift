//
//  ViewController.swift
//  HW-003-Youtube
//
//  Created by Ratanak Phang on 12/7/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit

struct CellData {
    var mainImage: UIImage!
    var channelImage: UIImage!
    var title: String!
    var channelName: String!
    var totalViews: String!
    var publishDate: String!
}

@IBDesignable
class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var ytMainTableView: UITableView!
    @IBOutlet weak var ytMainNavigationItem: UINavigationItem!
    
    var arrayCell = [CellData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Custom logo image
        let logoImage = UIImage(named: "youtube-logo")

        let scale = 110 / (logoImage?.size.width)!
        let newHeight = logoImage!.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: 110, height: newHeight))
        logoImage!.draw(in: CGRect(x: 0, y: 0, width: 110, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        let logoImageView = UIImageView(image: newImage)
        let imageBarItem = UIBarButtonItem(customView: logoImageView)
        ytMainNavigationItem.leftBarButtonItem = imageBarItem

        
        //Custom logo image
//        let logoImage = UIImage(named: "youtube-logo")
//
//        let logoImageView = UIImageView(image: logoImage)
//        logoImageView.frame = CGRect(x: 0, y: 0, width: 40, height: 10)
//
//
//
//
//        logoImageView.frame = CGRect(x: 0, y: 0, width: 80, height: 20)
//        logoImageView.contentMode = .scaleAspectFit
//        let imageBarItem = UIBarButtonItem(customView: logoImageView)
//
//        //let navigativeSpacer = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
//        //navigativeSpacer.width = -25
//        //ytMainNavigationItem.leftBarButtonItem = [navigativeSpacer, imageBarItem]
//        ytMainNavigationItem.leftBarButtonItem = imageBarItem
//
        
        // Get the superview's layout
        let margins = view.layoutMarginsGuide
     
        print("Pu Nak\(margins)")
        
        
        self.ytMainTableView.delegate = self
        self.ytMainTableView.dataSource = self
        arrayCell = [CellData(mainImage: #imageLiteral(resourceName: "image-love-rain-nikko"), channelImage: #imageLiteral(resourceName: "channel-nikko"), title: "Love Rain - Jang Geun Suk [Love Rain Ost] LYRICS", channelName: "nikko", totalViews: "55K views", publishDate: "5 years ago"),
            CellData(mainImage: #imageLiteral(resourceName: "image-dusk-till-dawn"), channelImage: #imageLiteral(resourceName: "channel-zayn"), title: "ZAYN - Dusk Till Dawn ft. Sia", channelName: "ZaynVEVO", totalViews: "472M views", publishDate: "3 months ago"),
                     CellData(mainImage: #imageLiteral(resourceName: "image-jonasblue"), channelImage: #imageLiteral(resourceName: "channel-jonasblue"), title: "Jonas Blue - Perfect Strangers ft. JP Cooper", channelName: "JonasBlueVEVO", totalViews: "370M views", publishDate: "1 year ago"),
                     CellData(mainImage: #imageLiteral(resourceName: "image-bad-day"), channelImage: #imageLiteral(resourceName: "channel-warner-bros-record"), title: "Daniel Powter - Bad Day (Official Music Video)", channelName: "Warner Bros. Records", totalViews: "90M views", publishDate: "8 years ago"),
                     CellData(mainImage: #imageLiteral(resourceName: "image-firebase-1"), channelImage: #imageLiteral(resourceName: "channel-firebase-1"), title: "iOS Swift Tutorial: Get started with Firebase and an App like TwitteriOS Swift Tutorial: Get started with Firebase and an App like Twitter", channelName: "Brian Advent", totalViews: "56k views", publishDate: "1 year ago"),
                     CellData(mainImage: #imageLiteral(resourceName: "image-firebase"), channelImage: #imageLiteral(resourceName: "channel-firebase"), title: "Firebase Tutorial for iOS - Ep 3 - Xcode Project Setup", channelName: "CodeWithChris", totalViews: "15k views", publishDate: "1 year ago"),
                    CellData(mainImage: #imageLiteral(resourceName: "image-lets-build-that-app"), channelImage: #imageLiteral(resourceName: "channel-lets-build-that-app"), title: "Swift: Lets Build YouTube - Home Feed: UICollectionView, AutoLayout (Ep 1)", channelName: "Lets Build That App", totalViews: "102k views", publishDate: "1 year ago")]
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCell.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("YTMainTableViewCell", owner: self, options: nil)?.first as! YTMainTableViewCell
        
        cell.ytMainImageView.image = arrayCell[indexPath.row].mainImage
        cell.ytMainChannelImageView.image = arrayCell[indexPath.row].channelImage
        cell.ytMainTitle.text = arrayCell[indexPath.row].title
        cell.ytMainChannel.text = arrayCell[indexPath.row].channelName
        cell.ytMainTotalViews.text = arrayCell[indexPath.row].totalViews
        cell.ytMainPublishDate.text = arrayCell[indexPath.row].publishDate
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // tableView.backgroundColor = UIColor.red
        // cell.backgroundColor = UIColor.green
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detailStoryBoard = storyBoard.instantiateViewController(withIdentifier: "DetailSB") as! DetailViewController
        //detailStoryBoard.getImage = arrayCell[indexPath.row].mainImage
        detailStoryBoard.detailContent = arrayCell[indexPath.row]
        self.navigationController?.pushViewController(detailStoryBoard, animated: true)
    }

}



