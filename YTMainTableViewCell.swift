//
//  YTMainTableViewCell.swift
//  HW-003-Youtube
//
//  Created by Ratanak Phang on 12/9/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit

class YTMainTableViewCell: UITableViewCell {

    @IBOutlet weak var ytMainImageView: UIImageView!
    @IBOutlet weak var ytMainChannelImageView: UIImageView!
    @IBOutlet weak var ytMainTitle: UILabel!
    @IBOutlet weak var ytMainChannel: UILabel!
    @IBOutlet weak var ytMainTotalViews: UILabel!
    @IBOutlet weak var ytMainPublishDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

@IBDesignable
class CustomUIImageView: UIImageView {
    @IBInspectable var circleImageView: CGFloat = 0 {
        didSet {
            layer.cornerRadius = circleImageView
            layer.masksToBounds = circleImageView > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
}
